<?php

namespace Noith\Payment\Providers;

use Illuminate\Support\ServiceProvider;
use Noith\Payment\Payment;

class PaymentServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(Payment::class, fn() => new Payment());
    }

    public function boot(): void
    {
        $this->publishes([
            __DIR__ . '/../database/migrations' => database_path('migrations')
        ], 'payment-migrations');
        $this->publishes([
            __DIR__ . '/../config/payments.php' => config_path('payments.php')
        ], 'payment-config');
    }
}
