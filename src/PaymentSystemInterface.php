<?php

namespace Noith\Payment;

use Illuminate\Http\Request;
use Noith\Payment\Models\PaymentInvoice;

interface PaymentSystemInterface
{
    public function createInvoice(PaymentInvoice $invoice): PaymentInvoice;

    public function getInvoiceStatus(PaymentInvoice $invoice): PaymentInvoice;

    public function processPayment(Request $request): PaymentInvoice;
}
