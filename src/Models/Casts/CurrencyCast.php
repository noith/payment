<?php

namespace Noith\Payment\Models\Casts;

use Akaunting\Money\Currency;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use UnexpectedValueException;

/**
 * @template TGet
 * @template TSet
 */
class CurrencyCast implements CastsAttributes
{
    public function get($model, string $key, $value, array $attributes): ?Currency
    {
        if (is_null($value)) {
            return null;
        }
        if (!is_string($value)) {
            throw new UnexpectedValueException;
        }

        return new Currency($value);
    }

    public function set($model, string $key, $value, array $attributes): string
    {
        if (!$value instanceof Currency) {
            $value = new Currency($value);
        }
        return $value->getCurrency();
    }
}
