<?php

namespace Noith\Payment\Models\Casts;

use Akaunting\Money\Money;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class MoneyCast implements CastsAttributes
{
    public function __construct(protected string $column = 'currency')
    {

    }

    public function get($model, string $key, $value, array $attributes): ?Money
    {
        return is_null($value) ? null : new Money(
            $value,
            $model->{$this->column}
        );
    }

    public function set($model, string $key, $value, array $attributes): string
    {
        if (!$value instanceof Money) {
            throw new \UnexpectedValueException();
        }

        return $value->getAmount();
    }
}
