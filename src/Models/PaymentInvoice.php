<?php

namespace Noith\Payment\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Noith\Payment\Enum\PaymentStatus;
use Noith\Payment\Models\Casts\CurrencyCast;
use Noith\Payment\Models\Casts\MoneyCast;

class PaymentInvoice extends Model
{
    use SoftDeletes;

    protected $guarded = ['uuid'];

    protected $primaryKey = 'uuid';
    public $incrementing = false;

    protected $keyType = 'string';

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s',
        'paid_at' => 'datetime:Y-m-d H:i:s',
        'status' => PaymentStatus::class,
        'payload' => 'array',
        'additional_info' => 'array',
        'currency' => CurrencyCast::class,
        'amount' => MoneyCast::class,
    ];

    public static function boot(): void
    {
        parent::boot();

        self::creating(function ($model) {
            $model->uuid = Str::uuid()->toString();
        });
    }

    public function object(): MorphTo
    {
        return $this->morphTo();
    }
}
