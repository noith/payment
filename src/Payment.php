<?php

namespace Noith\Payment;

use Akaunting\Money\Money;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Noith\Payment\Enum\PaymentStatus;
use Noith\Payment\Events\PaymentInvoiceCreatedEvent;
use Noith\Payment\Events\PaymentInvoiceStatusChangedEvent;
use Noith\Payment\Exceptions\PaymentException;
use Noith\Payment\Models\PaymentInvoice;

class Payment
{
    /**
     * @throws PaymentException
     */
    public function createInvoice(string $paymentSystem, Model $object, Money $amount, string $description, array $additionalInfo = []): PaymentInvoice
    {
        $invoice = PaymentInvoice::create([
            'payment_system' => $paymentSystem,
            'status' => PaymentStatus::Created,
            'amount' => $amount,
            'currency' => $amount->getCurrency(),
            'object_id' => $object->getKey(),
            'object_type' => $object::class,
            'description' => $description,
            'additional_info' => $additionalInfo,
        ]);
        PaymentInvoiceCreatedEvent::dispatch($invoice);
        $this->getHandler($paymentSystem)->createInvoice($invoice);
        PaymentInvoiceStatusChangedEvent::dispatch($invoice);
        return $invoice;
    }

    /**
     * @throws PaymentException
     */
    public function getHandler(string $paymentSystem): PaymentSystemInterface
    {
        $handler = config('payments.' . $paymentSystem . '.handler');
        if (!$handler) {
            throw new PaymentException('No handler for payment system: ' . $paymentSystem);
        }
        return new $handler();
    }

    /**
     * @throws ModelNotFoundException|PaymentException
     */
    public function getInvoiceStatus(string $invoiceId): PaymentInvoice
    {
        /** @var PaymentInvoice $invoice */
        $invoice = PaymentInvoice::query()->where('uuid', '=', $invoiceId)->firstOrFail();
        $handler = $this->getHandler($invoice->payment_system);
        $currentStatus = $invoice->status;
        $invoice = $handler->getInvoiceStatus($invoice);
        if ($currentStatus !== $invoice->status) {
            PaymentInvoiceStatusChangedEvent::dispatch($invoice);
        }
        return $invoice;
    }

    /**
     * @throws PaymentException
     */
    public function processPayment(string $paymentSystem, Request $request): PaymentInvoice
    {
        $handler = $this->getHandler($paymentSystem);
        $invoice = $handler->processPayment($request);
        PaymentInvoiceStatusChangedEvent::dispatch($invoice);
        return $invoice;
    }
}
