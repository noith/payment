<?php

namespace Noith\Payment\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Noith\Payment\Models\PaymentInvoice;

class PaymentInvoiceCreatedEvent
{
    use Dispatchable;

    public function __construct(public PaymentInvoice $invoice)
    {
    }
}
