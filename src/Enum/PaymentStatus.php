<?php

namespace Noith\Payment\Enum;

enum PaymentStatus: string
{
    case Created = 'created';
    case Checkout = 'checkout';
    case Paid = 'paid';
    case PartiallyPaid = 'partially_paid';
    case PartiallyPaidExpired = 'partially_paid_expired';
    case Expired = 'expired';
    case Cancelled = 'cancelled';
    case Success = 'success';
    case Unknown = 'unknown';
}
