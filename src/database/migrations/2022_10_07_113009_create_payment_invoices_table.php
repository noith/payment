<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('payment_invoices', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('payment_system');
            $table->integer('amount');
            $table->string('currency');
            $table->string('external_id')->nullable();
            $table->string('status');
            $table->morphs('object');
            $table->string('description')->nullable();
            $table->string('link')->nullable();
            $table->json('additional_info')->nullable();
            $table->json('payload')->nullable();
            $table->timestamp('paid_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('payment_invoices');
    }
};
